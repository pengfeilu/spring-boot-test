package com.lupf.springboottest.utils.validator;

/**
 * 验证分组接口
 */
public class ValidationGroup {
    /**
     * 注册的分组
     */
    public interface Register {
    }

    /**
     * 登录的分组
     */
    public interface Login {
    }
}

package com.lupf.springboottest.utils.validator.myvalidator;

/**
 * 字母大小写枚举
 */
public enum CaseMode {
    //大写
    UPPER,
    //小写
    LOWER;
}

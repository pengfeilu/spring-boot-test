package com.lupf.springboottest.utils.validator;

import com.lupf.springboottest.error.BusiErrCodeEm;
import com.lupf.springboottest.error.BusiException;
import org.hibernate.validator.HibernateValidator;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

//把这个工具类交由Spring管理
@Component
public class ValidatorUtil implements InitializingBean {

    private Validator validator;

    /**
     * 通过Validator校验对象
     *
     * @param object
     * @param groups
     * @return
     */
    public ValidationResult validata(Object object, Class<?>... groups) {
        ValidationResult validationResult = new ValidationResult();
        Set<ConstraintViolation<Object>> constraintViolationSet = validator.validate(object, groups);
        if (constraintViolationSet.size() > 0) {
            constraintViolationSet.forEach(constraintViolation -> {
                validationResult.setHasError(true);
                String msg = constraintViolation.getMessage();
                String propertyName = constraintViolation.getPropertyPath().toString();
                validationResult.getErrMsgMap().put(propertyName, msg);
            });
        }
        return validationResult;
    }

    /**
     * 校验请求参数对象，如果出现未校验通过的，直接抛出异常
     *
     * @param object 待处理的对象
     * @param groups 校验分组
     * @throws BusiException
     */
    public void reqValidata(Object object, Class<?>... groups) throws BusiException {
        ValidationResult validationResult = this.validata(object, groups);
        if (null != validationResult && validationResult.getHasError()) {
            //请求参数存在不合法的数据，这里直接抛出异常
            throw new BusiException(BusiErrCodeEm.REQ_PARAM_10001, validationResult.getErrMsg());
        }
    }

    @Override
    public void afterPropertiesSet() {
        //failFast(true) true:快速校验，遇到不合法的就直接返回  false:全量校验，找出所有不合法的数据
        validator = Validation.byProvider(HibernateValidator.class).configure().failFast(true).buildValidatorFactory().getValidator();
    }
}

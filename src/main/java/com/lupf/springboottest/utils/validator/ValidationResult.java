package com.lupf.springboottest.utils.validator;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 验证的响应对象
 */
public class ValidationResult {
    /**
     * 是否存在错误
     */
    private Boolean hasError = false;

    /**
     * 错误的信息的map
     */
    private Map<String, String> errMsgMap = new HashMap<>();

    public Boolean getHasError() {
        return hasError;
    }

    public void setHasError(Boolean hasError) {
        this.hasError = hasError;
    }

    public Map<String, String> getErrMsgMap() {
        return errMsgMap;
    }

    public void setErrMsgMap(Map<String, String> errMsgMap) {
        this.errMsgMap = errMsgMap;
    }

    /**
     * 用于获取校验之后错误描述的信息
     *
     * @return
     */
    public String getErrMsg() {
        if (null != this.errMsgMap && this.errMsgMap.size() > 0)
            return StringUtils.join(this.errMsgMap.values().toArray(), ",");
        return null;
    }
}

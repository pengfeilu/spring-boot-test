package com.lupf.springboottest.utils.redis;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashSet;
import java.util.Set;

public class RedisUtil {
    public void getHelper() {
        //相关代码如下：

        JedisPoolConfig config = new JedisPoolConfig();
        //config = new JedisPoolConfig();
        config.setMaxTotal(60000);//设置最大连接数  
        config.setMaxIdle(1000); //设置最大空闲数 
        config.setMaxWaitMillis(3000);//设置超时时间
        config.setTestOnBorrow(true);

        // 集群结点
        Set<HostAndPort> jedisClusterNode = new HashSet<HostAndPort>();
        jedisClusterNode.add(new HostAndPort("192.168.1.208", 7000));
        jedisClusterNode.add(new HostAndPort("192.168.1.208", 7001));
        jedisClusterNode.add(new HostAndPort("192.168.1.208", 7002));
        jedisClusterNode.add(new HostAndPort("192.168.1.208", 7003));
        jedisClusterNode.add(new HostAndPort("192.168.1.208", 7004));
        jedisClusterNode.add(new HostAndPort("192.168.1.208", 7005));

        JedisCluster jc = new JedisCluster(jedisClusterNode, 10000, 5000, 1, "123456789", new GenericObjectPoolConfig());
//        JedisCluster jc  = new JedisCluster(jedisClusterNode,new GenericObjectPoolConfig());
//        jc.auth("123456789");
        //JedisCluster jc = new JedisCluster(jedisClusterNode);
        jc.set("name", "zhangsan");
        String value = jc.get("name");
        System.out.println(value);
    }

}

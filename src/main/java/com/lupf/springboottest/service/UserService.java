package com.lupf.springboottest.service;

import com.lupf.springboottest.error.BusiException;
import com.lupf.springboottest.service.model.UserModel;
import org.apache.catalina.User;

/**
 * 用户的service接口
 */
public interface UserService {
    /**
     * 根据用户id获取用户对象
     *
     * @param id
     */
    UserModel getUserBuId(Integer id) throws BusiException;

    /**
     * 获取手机验证码
     *
     * @param phoneNum 用户手机号码
     */
    void getUserPhoneCode(String phoneNum) throws BusiException;

    /**
     * 注册
     *
     * @param userModel
     * @return
     */
    UserModel register(UserModel userModel) throws BusiException;

    /**
     * 登录
     *
     * @param phoneNum       电话
     * @param encrptPassword 密码
     * @return
     */
    UserModel login(String phoneNum, String encrptPassword) throws BusiException;
}

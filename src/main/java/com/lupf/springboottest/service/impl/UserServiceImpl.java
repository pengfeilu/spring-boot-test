package com.lupf.springboottest.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.lupf.springboottest.dao.UserDOMapper;
import com.lupf.springboottest.dao.UserPasswordDOMapper;
import com.lupf.springboottest.dataobject.UserDO;
import com.lupf.springboottest.dataobject.UserPasswordDO;
import com.lupf.springboottest.error.BusiErrCodeEm;
import com.lupf.springboottest.error.BusiException;
import com.lupf.springboottest.service.UserService;
import com.lupf.springboottest.service.model.UserModel;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Random;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDOMapper userDOMapper;

    @Autowired
    private UserPasswordDOMapper userPasswordDOMapper;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Override
    public UserModel getUserBuId(Integer id) throws BusiException {
        //根据用户id获取用户信息
        UserDO userDO = userDOMapper.selectByPrimaryKey(id);

        //根据用户的id获取到用户的密码信息
        UserPasswordDO userPasswordDO = null;
        if (null != userDO) {
            userPasswordDO = userPasswordDOMapper.selectByUserId(userDO.getId());
        }

        //将数据模型对象转换为领域模型对象
        UserModel userModel = userDOAndPasswordDO2UserModel(userDO, userPasswordDO);

        //返回用户信息
        return userModel;
    }

    @Override
    public void getUserPhoneCode(String phoneNum) throws BusiException {
        //校验手机号码的有效性
        //这里就不演示具体的校验规则了，网上很多

        //生成验证码
        Random random = new Random();
        //生个6位的随机数
        int code = random.nextInt(899999) + 100000;

        //发送验证码
        //这里发送验证码；可以使用三方的接口将验证码发送给用户
        //演示使用，这里就只做个打印
        System.out.println("验证码为:" + code);

        //保存验证码
        //为了方便测试，这里将验证码放在session里面
        //实际的开发过程中，可以将这个数据保存到redis中并且给这个数据设置一个有效期
        httpServletRequest.getSession().setAttribute(phoneNum, code);
    }

    @Override
    @Transactional
    public UserModel register(UserModel userModel) throws BusiException {
        //校验前端上传的用户信息
        if (null == userModel) {
            throw new BusiException(BusiErrCodeEm.REQ_PARAM_10001);
        }

        //校验密码的合法性
        //这里按各自的规则进行校验即可
        //这里不做演示

        UserDO userDO = userModel2UserDO(userModel);
        try {
            //保存用户
            //在mapper对应的方法下添加以下参数
            //这里很重要，否则添加之后userDO对象里面的id没有数据
            //keyProperty="id" useGeneratedKeys="true"
            userDOMapper.insertSelective(userDO);

            //保存密码
            //组装用户密码相关的对象
            UserPasswordDO userPasswordDO = new UserPasswordDO();
            //用户的索引ID
            userPasswordDO.setUserId(userDO.getId());
            //加密后的密码
            userPasswordDO.setEncrptPassword(userModel.getEncrptPassword());
            //添加用户密码数据
            userPasswordDOMapper.insertSelective(userPasswordDO);
        } catch (DuplicateKeyException e) {
            //用户号码是唯一键，如果存在，说明用户已经注册过了
            e.printStackTrace();
            throw new BusiException(BusiErrCodeEm.USER_20003);
        }

        //根据ID，重新获取一遍注册的用户信息并返回
        UserModel inserUserModel = this.getUserBuId(userDO.getId());

        //返回用户信息
        return inserUserModel;
    }

    @Override
    public UserModel login(String phoneNum, String encrptPassword) throws BusiException {
        //获取用户信息
        //根据用户的电话号码获取用户ID
        UserDO userDO = userDOMapper.selectByPhoneNum(phoneNum);

        //定义用户密码信息数据对象
        UserPasswordDO userPasswordDO = null;

        //判断该用户是否已经
        if (null != userDO) {
            userPasswordDO = userPasswordDOMapper.selectByUserId(userDO.getId());
        } else {
            throw new BusiException(BusiErrCodeEm.USER_20004);
        }

        //校验用户密码
        if (null == userPasswordDO || !StringUtils.equals(encrptPassword, userPasswordDO.getEncrptPassword())) {
            //这里提示用户名或者密码不正确是为了防止用户去不停尝试帐号密码
            throw new BusiException(BusiErrCodeEm.USER_20001, "用户名或者密码不正确");
        }

        //校验成功
        //将数据对象转换为领域模型对象
        UserModel userModel = this.userDOAndPasswordDO2UserModel(userDO, userPasswordDO);
        //返回用户信息
        return userModel;
    }

    /**
     * 将用户的DO对象和密码DO对象转换为用户业务对象
     *
     * @param userDO
     * @param userPasswordDO
     * @return
     */
    protected UserModel userDOAndPasswordDO2UserModel(UserDO userDO, UserPasswordDO userPasswordDO) {
        if (null == userDO)
            return null;

        UserModel userModel = new UserModel();
        BeanUtils.copyProperties(userDO, userModel);
        if (null != userPasswordDO) {
            userModel.setEncrptPassword(userPasswordDO.getEncrptPassword());
        }
        return userModel;
    }

    /**
     * 将领域模型对象转换为数据对象
     *
     * @param userModel
     * @return
     */
    public UserDO userModel2UserDO(UserModel userModel) {
        UserDO userDO = new UserDO();
        BeanUtils.copyProperties(userModel, userDO);
        return userDO;
    }
}

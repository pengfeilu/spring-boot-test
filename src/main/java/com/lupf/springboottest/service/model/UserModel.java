package com.lupf.springboottest.service.model;

import com.lupf.springboottest.utils.validator.ValidationGroup;
import com.lupf.springboottest.utils.validator.myvalidator.CaseCheck;
import com.lupf.springboottest.utils.validator.myvalidator.CaseMode;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;

/**
 * 用户的业务对象
 */
public class UserModel {
    private Integer id;

    @NotBlank(message = "用户昵称不能为空", groups = ValidationGroup.Register.class)
    private String name;

    @NotNull(message = "性别选择不能为空", groups = ValidationGroup.Register.class)
    private Byte sex;

    @Max(value = 200, message = "年龄不能大于200岁", groups = ValidationGroup.Register.class)
    @Min(value = 0, message = "年龄不能小于0岁", groups = ValidationGroup.Register.class)
    private Integer age;

    @NotBlank(message = "手机号码不能为空", groups = {ValidationGroup.Login.class, ValidationGroup.Register.class})
    @Pattern(regexp = "^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199|(147))\\d{8}$", message = "号码格式不正确!", groups = {ValidationGroup.Login.class, ValidationGroup.Register.class})
    private String telphone;

    @Email(message = "邮箱格式错误", groups = ValidationGroup.Register.class)
    private String email;

    @CaseCheck(value = CaseMode.UPPER, message = "注册方式必须是大写字母", groups = ValidationGroup.Register.class)
    private String registerMode;

    private String thirdPartyId;

    @URL(message = "头像必须是链接地址", groups = ValidationGroup.Register.class)
    private String avatar;

    //这里是定义的用户加密后密码
    //由于DAO层
    //由于DO层用户信息和用户密码是分开的 但是从业务层面来看，这个密码确实是用户一部分，因此在这里的业务对象将用户信息和密码信息合并
    //这个规则是校验明文的
    @Length(min = 6, max = 18, message = "密码长度为6-18个字符", groups = {ValidationGroup.Login.class, ValidationGroup.Register.class})
    private String encrptPassword;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Byte getSex() {
        return sex;
    }

    public void setSex(Byte sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRegisterMode() {
        return registerMode;
    }

    public void setRegisterMode(String registerMode) {
        this.registerMode = registerMode;
    }

    public String getThirdPartyId() {
        return thirdPartyId;
    }

    public void setThirdPartyId(String thirdPartyId) {
        this.thirdPartyId = thirdPartyId;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEncrptPassword() {
        return encrptPassword;
    }

    public void setEncrptPassword(String encrptPassword) {
        this.encrptPassword = encrptPassword;
    }
}

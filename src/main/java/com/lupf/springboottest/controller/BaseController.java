package com.lupf.springboottest.controller;

import com.lupf.springboottest.error.BusiErrCodeEm;
import com.lupf.springboottest.error.BusiException;
import com.lupf.springboottest.response.BaseRespObj;
import com.lupf.springboottest.utils.validator.ValidatorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class BaseController {

    @Autowired
    public ValidatorUtil validatorUtil;

    /**
     * 通过ExceptionHandler 捕获controller未处理的异常，给用户一个友好的返回
     *
     * @param ex 异常信息
     * @return
     */
    //捕获为Exception的异常
    @ExceptionHandler(Exception.class)
    //指明响应的状态为200
    @ResponseStatus(HttpStatus.OK)
    //返回业务对象
    @ResponseBody
    public Object exceptionHandler(Exception ex) {
        BaseRespObj baseRespObj = new BaseRespObj();
        baseRespObj.setStatus(-1);
        Map<String, Object> errObjs = new HashMap<>();
        if (ex instanceof BusiException) {
            BusiException busiException = (BusiException) ex;
            errObjs.put("errCode", busiException.getErrCode());
            errObjs.put("errMsg", busiException.getErrMsg());
        } else {
            errObjs.put("errCode", BusiErrCodeEm.UNKNOWN.getErrCode());
            errObjs.put("errMsg", BusiErrCodeEm.UNKNOWN.getErrMsg());
        }
        baseRespObj.setData(errObjs);
        return baseRespObj;
    }
}

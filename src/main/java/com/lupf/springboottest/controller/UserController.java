package com.lupf.springboottest.controller;


import com.lupf.springboottest.controller.viewobject.UserVO;
import com.lupf.springboottest.dao.UserDOMapper;
import com.lupf.springboottest.error.BusiErrCodeEm;
import com.lupf.springboottest.error.BusiException;
import com.lupf.springboottest.response.BaseRespObj;
import com.lupf.springboottest.service.UserService;
import com.lupf.springboottest.service.model.UserModel;
import com.lupf.springboottest.utils.validator.ValidationGroup;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sun.misc.BASE64Encoder;

import javax.servlet.http.HttpServletRequest;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    @Autowired
    UserDOMapper userDOMapper;


    @Autowired
    private UserService userService;

    @Autowired
    private HttpServletRequest httpServletRequest;

    /**
     * 获取用户验证码
     *
     * @param phoneNum 用户号码
     * @return
     * @throws BusiException
     */
    @RequestMapping(value = "/getPhoneCode", method = RequestMethod.POST)
    public BaseRespObj getUserByID(@RequestParam(name = "phoneNum") String phoneNum) throws BusiException {
        userService.getUserPhoneCode(phoneNum);
        return BaseRespObj.create(null);
    }

    /**
     * 注册
     *
     * @param name     用户昵称
     * @param phoneNum 用户号码
     * @param code     用户验证码
     * @param age      年龄
     * @param sex      性别
     * @param email    邮箱
     * @param pwd      密码
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public BaseRespObj register(
            @RequestParam(name = "name") String name,
            @RequestParam(name = "phoneNum") String phoneNum,
            @RequestParam(name = "phoneCode") Integer code,
            @RequestParam(name = "age") Integer age,
            @RequestParam(name = "sex") String sex,
            @RequestParam(name = "email") String email,
            @RequestParam(name = "pwd") String pwd,
            @RequestParam(name = "avatar") String avatar,
            @RequestParam(name = "registerMode") String registerMode) throws Exception {

        //将前端数据转换为领域模型对象
        UserModel userModel = new UserModel();
        userModel.setAge(age);
        userModel.setSex(Byte.valueOf(sex));
        userModel.setEmail(email);
        userModel.setName(name);
        userModel.setTelphone(phoneNum);
        //先设置明文进行校验
        userModel.setEncrptPassword(pwd);
        userModel.setAvatar(avatar);
        userModel.setRegisterMode(registerMode);
        validatorUtil.reqValidata(userModel, ValidationGroup.Register.class);

        //在session中取出前面发送的验证码
        Integer sessCode = (Integer) httpServletRequest.getSession().getAttribute(phoneNum);

        if (null == sessCode || sessCode.intValue() != code.intValue()) {
            throw new BusiException(BusiErrCodeEm.REQ_PARAM_10001, "短信验证码信息有误！");
        }

        userModel.setEncrptPassword(this.encodeByMD5(pwd));
        userModel.setRegisterMode("bytelphone");
        UserModel insertUserModel = userService.register(userModel);
        UserVO userVO = userModel2UserVO(insertUserModel);

        return BaseRespObj.create(userVO);
    }

    /**
     * 登录
     *
     * @param phoneNum 用户电话号码
     * @param pwd      用户密码
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public BaseRespObj login(
            @RequestParam(name = "phoneNum") String phoneNum,
            @RequestParam(name = "pwd") String pwd) throws Exception {

        if (StringUtils.isEmpty(phoneNum) || StringUtils.isEmpty(pwd)) {
            throw new BusiException(BusiErrCodeEm.REQ_PARAM_10001);
        }

        UserModel userModel = userService.login(phoneNum, this.encodeByMD5(pwd));
        UserVO userVO = userModel2UserVO(userModel);
        return BaseRespObj.create(userVO);
    }


    /**
     * 将用户业务对象转为前端的视图对象
     *
     * @param userModel 业务模型对象
     * @return 前端视图对象
     */
    protected UserVO userModel2UserVO(UserModel userModel) {
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(userModel, userVO);
        return userVO;
    }

    /**
     * MD5加密
     *
     * @param va 待加密的数据
     * @return 加密后的数据
     * @throws NoSuchAlgorithmException
     */
    protected String encodeByMD5(String va) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        BASE64Encoder base64Encoder = new BASE64Encoder();
        String newVa = base64Encoder.encode(md5.digest(va.getBytes()));
        return newVa;
    }
}

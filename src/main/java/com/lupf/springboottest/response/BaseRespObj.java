package com.lupf.springboottest.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * 平台基础响应对象
 */

@JsonSerialize(include= JsonSerialize.Inclusion.NON_NULL)
public class BaseRespObj {
    //响应状态
    //0:成功
    //-1:失败
    private Integer status;

    //响应数据
    //status==0的时候，这里返回的是业务数据
    //status==-1的时候，这里返回的是错误码信息
    private Object data;

    //静态构造，只传data默认为成功
    public static BaseRespObj create(Object data) {
        return BaseRespObj.create(data, 1);
    }

    public static BaseRespObj create(Object data, Integer status) {
        BaseRespObj baseRespObj = new BaseRespObj();
        if (null != data)
            baseRespObj.setData(data);
        baseRespObj.setStatus(status);
        return baseRespObj;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}

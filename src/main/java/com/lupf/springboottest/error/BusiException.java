package com.lupf.springboottest.error;

import org.apache.commons.lang3.StringUtils;

public class BusiException extends Exception implements ComErrorInf {
    private ComErrorInf comErrorInf;

    /**
     * 接受BusiErrCodeEm传参，构造业务异常
     *
     * @param comErrorInf
     */
    public BusiException(ComErrorInf comErrorInf) {
        super();
        this.comErrorInf = comErrorInf;
    }

    public BusiException(ComErrorInf comErrorInf, String errMsg) {
        super();
        this.comErrorInf = comErrorInf;
        if (StringUtils.isNotEmpty(errMsg)) {
            this.comErrorInf.setErrMsg(errMsg);
        }
    }

    @Override
    public Integer getErrCode() {
        return this.comErrorInf.getErrCode();
    }

    @Override
    public String getErrMsg() {
        return this.comErrorInf.getErrMsg();
    }

    @Override
    public ComErrorInf setErrMsg(String errMsg) {
        if (StringUtils.isNotEmpty(errMsg)) {
            this.comErrorInf.setErrMsg(errMsg);
        }
        return this;
    }
}

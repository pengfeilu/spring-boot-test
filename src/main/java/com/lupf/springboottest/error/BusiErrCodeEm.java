package com.lupf.springboottest.error;

public enum BusiErrCodeEm implements ComErrorInf {
    //>10000的错误码约束为参数才错误码
    REQ_PARAM_10001(10001, "参数信息有误"),
    REQ_PARAM_10002(10002, "参数校验失败"),

    //>20000 约束为用户的错误码
    USER_20001(20001, "用户相关错误"),
    USER_20002(20002, "未找到用户"),
    USER_20003(20003, "用户信息已存在"),
    USER_20004(20004, "该用户未注册"),

    //>30000 约束为订单的错误码
    BUSI_ORDER_30001(30001, "订单信息未找到"),

    UNKNOWN(999999, "未知错误");
    /**
     * 错误码
     */
    private Integer errCode;

    /**
     * 错误描述
     */
    private String errMsg;

    BusiErrCodeEm(Integer errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    @Override
    public Integer getErrCode() {
        return this.errCode;
    }

    @Override
    public String getErrMsg() {
        return this.errMsg;
    }

    @Override
    public ComErrorInf setErrMsg(String errMsg) {
        this.errMsg = errMsg;
        return this;
    }
}

package com.lupf.springboottest.error;

/**
 * 公共异常的接口
 */
public interface ComErrorInf {
    /**
     * 获取错误码
     *
     * @return
     */
    Integer getErrCode();

    /**
     * 获取错误描述
     *
     * @return
     */
    String getErrMsg();

    /**
     * 修改错误描述信息
     *
     * @param errMsg
     * @return
     */
    ComErrorInf setErrMsg(String errMsg);
}

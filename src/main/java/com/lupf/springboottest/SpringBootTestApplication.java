package com.lupf.springboottest;

import com.lupf.springboottest.utils.redis.RedisUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//指定springBoot扫描的整个项目的父路径，即为在改路径下的所有方法将都会被扫描注入
@SpringBootApplication(scanBasePackages = {"com.lupf.springboottest"})
//指定当前项目为Eureka客户端
@EnableEurekaClient
//mybatis mapper扫描的dao路径
@MapperScan("com.lupf.springboottest.dao")
@RestController
public class SpringBootTestApplication {

//    @Autowired
//    StringRedisTemplate redisTemplate;

//    ValueOperations<String, String> stringRedis;

    public static void main(String[] args) {
        SpringApplication.run(SpringBootTestApplication.class, args);
    }

//    @PostConstruct
//    public void init() {
//        stringRedis = redisTemplate.opsForValue();
//    }

    @RequestMapping(value = "/set", method = RequestMethod.POST)
    public boolean redisTestSet(@RequestParam(name = "key") String key, @RequestParam(name = "va") String va) {
//        stringRedis.set(key, va);
        RedisUtil redisUtil = new RedisUtil();
        redisUtil.getHelper();
        return true;
    }

    @RequestMapping(value = "/get",method = RequestMethod.GET)
    public String redisTestGet(@RequestParam(name = "key") String key) {
//        String va = stringRedis.get(key);
        return "";
    }
}
